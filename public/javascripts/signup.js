/**
 * Created by paulfrohling on 08.03.15.
 */
var signUpManager = {};
var inputs = {};

signUpManager.pwIsValidated = false;

signUpManager.init = function(){

    inputs.firstname = $("#firstname");
    inputs.surname = $("#surname");
    inputs.email = $("#email");
    inputs.password = $("#password");
    inputs.confirmedPassword = $("#confirmPassword");
    inputs.pwValidatorImg = $("#pwValidatorMark");

    signUpManager.bindConfirmPWEvent();

    $("#signUpButton").bind("click", function(){
        signUpManager.sendData();
    });

    inputs.firstname.bind("keyup",function(){
        signUpManager.validateInputs()
    });

};

signUpManager.bindConfirmPWEvent = function(){

    inputs.confirmedPassword.bind("keyup", function(){
        if(inputs.confirmedPassword.val() != inputs.password.val()){
            inputs.confirmedPassword.css("border-color","#f06f6d");
            inputs.pwValidatorImg.css("background-color","#f06f6d")
        }else{
            inputs.confirmedPassword.css("border-color","#57989f");
            inputs.pwValidatorImg.css("background-color","#57989f")
            signUpManager.pwIsValidated = true;
        }
    });

};

signUpManager.sendData = function(){
    if(signUpManager.checkInputs && signUpManager.pwIsValidated){
        console.log("no problem");
        $.ajax({
            url:"/createUser",
            method:"post",
            contentType:"Application/Json",
            data:
                JSON.stringify({firstname: inputs.firstname.val(),
                    surname:inputs.surname.val(),
                    email: inputs.email.val(),
                    password:inputs.password.val()
                }),
            success:function(data,err){
                console.log(data);
                if(data){
                    $("#signupForm").toggle("scale");
                }
            }

        });

    }else{
        console.log("problem");
    }
};

signUpManager.checkInputs = function(){
    if(inputs.firstname.val() != undefined){
        if(inputs.surname.val != undefined){
            if(inputs.email.val != undefined){
              if(inputs.password.val()!=undefined){
                  return true;
              }
            }
        }
    }else{
        return false;
    }
};

signUpManager.validateInputs = function(){
  var regExp = /[^a-zA-z]/g;
  if(!inputs.firstname.val().match(regExp)){
      console.log("Nothing wrong");
  }else{
      console.log("There is a non letter digit...");
  }
};

