/**
 * Created by paulfrohling on 08.03.15.
 */
var IGNApp = {};
var StatusFunctions = {};


IGNApp.init = function(){
    IGNApp.websocket = new WebSocket("ws://localhost:8181/", "ign");

    IGNApp.websocket.onmessage = function (e) {
        var message = JSON.parse(e.data);
        console.log(message);
        if(IGNApp.FunctionForMsgType[message.type] != undefined){
            IGNApp.FunctionForMsgType[message.type](message);
        }

    };

    IGNApp.websocket.onopen = function(){
        $("#browserIcon").attr("src","./images/browserGreen.png");
        IGNApp.websocket.send(JSON.stringify({type:"BtSR"})); //Browser to Server Request
    };

    IGNApp.websocket.onclose = function(){
        $("#browserIcon").attr("src","./images/browser.png");
    };
};


//Server to Browser Answer
StatusFunctions.StBA = function(message){
    console.log("CALL");
    IGNApp.switchIPadConnectionIcon(message.status);
};

//IPad to Browser Status Message
StatusFunctions.ItBSM = function(message){
    console.log("ITBSM");
    IGNApp.switchIPadConnectionIcon(message.status);
};


IGNApp.switchIPadConnectionIcon = function(isConnected){
    if(isConnected){
        $("#iPadIcon").attr("src","./images/iPadGreen.png");
    }else{
        $("#iPadIcon").attr("src","./images/ipad.png");
    }
};

IGNApp.FunctionForMsgType = {StBA:StatusFunctions.StBA, ItBSM: StatusFunctions.ItBSM};


