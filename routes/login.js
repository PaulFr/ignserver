/**
 * Created by paulfrohling on 04.03.15.
 */
var db = require("mysql");
var cuid = require("cuid");
var LoginManager = {};
var sha1 = require("sha1");

var connection = db.createConnection({
    host:"localhost",
    user:"IGNServer",
    database:"IGNData",
    password:"blablub1478"
});

LoginManager.loginUser = function(req,res){
    var email = LoginManager.removeTicks(connection.escape(req.body.email));
    var pw =  sha1(LoginManager.removeTicks(connection.escape(req.body.password)));
    var query = "SELECT*FROM users WHERE `email`='" + email+"' AND `password`='"+pw+"'";
    console.log(query);
    console.log(connection.escape(req.body.password));

    connection.query(query, function(err, results){
        if(err){
            throw err;
            res.status(500).send("Something stupid happened");
        }else{
            if(results.length == 0){
                res.status(404).send("No users found");
            }else{
                console.log(results);
                req.session.user = results[0].uuid ;
                console.log("user found");
                res.redirect("/IGNApp");
            }
        }
    });
}

LoginManager.createUser = function(req, res){
    var newCuid = cuid();
    var data = {
        "firstname":LoginManager.removeTicks(connection.escape(req.body.firstname)),
        "surname":LoginManager.removeTicks(connection.escape(req.body.surname)),
        "email": LoginManager.removeTicks(connection.escape(req.body.email)),
        "password": sha1(LoginManager.removeTicks(connection.escape(req.body.password))),
        "isAuthenticated": "false",
        "uuid":newCuid
    };
    console.log(data.password);
    var query = "INSERT INTO `users` SET ?"
    connection.query(query,data, function(err, result){
       if(err){
           res.status(500).send("Something stupid happened");
           throw err;
       }else{
           res.status(200).send("User successfully created");
       }
    });

};

LoginManager.logoutUser = function(req, res){
    req.session.destroy(function(err) {
       if(!err){
           res.redirect("/");
       }
    });
};

LoginManager.removeTicks = function(string){
    var newString = string.replace(/'/g, "");
    console.log(newString);
    return newString;
};


module.exports = LoginManager;