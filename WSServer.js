/**
 * Created by paulfrohling on 06.03.15.
 */
var ws = require("websocket"),
    http = require("http"),
    signature = require('cookie-signature'),
    addition = {};

addition.unsignSignedCookie = function(str, secret){
    return str.substr(0, 2) === 's:'
        ? signature.unsign(str.slice(2), secret)
        : str;
};

var myWSModule ={};

myWSModule.init = function(store){

   myWSModule.sessionStore = store;

   myWSModule.server = http.createServer();
   myWSModule.server.listen(8181);

   myWSModule.WSServer = new ws.server({
        httpServer:myWSModule.server,
        autoAcceptConnection:false
   });


    myWSModule.connections = {};

        myWSModule.WSServer.on("request", function(request){
        var cookie = myWSModule.findCookie(request.cookies, "connect.sid");
        var clientType = myWSModule.getClientType(request);
        console.log("CLIENT TYPE: "+clientType);
        if(cookie != null){
          var cookieUnsigned = addition.unsignSignedCookie(cookie.value,
                                                   "paulscbgthisblabionadelemon");

          myWSModule.sessionStore.get(cookieUnsigned, function(err, res){
              if(res != undefined){
                  var connection = request.accept("ign", request.origin);
                  connection.send(JSON.stringify({msg:"hallo"}));
                  connection.ignUser = res.user;
                  if(clientType == "iPad"){
                      myWSModule.addConnection(res.user,connection, "iPad");
                      myWSModule.createConnectionBindings(connection, "iPad");
                  }else if(clientType == null){
                      myWSModule.addConnection(res.user,connection, "browser");
                      myWSModule.createConnectionBindings(connection, "browser");
                  }
                  console.log("Websocket established");
              }else{
                  request.reject("404","Please log in first!");
              }
          });
        }else{
          request.reject("404","Please log in first!");
          console.log("no user logged in");
        }
    });
};



myWSModule.addConnection = function(user, connection, clientType){
    if(myWSModule.connections[user] == undefined){
        if(clientType == "browser"){
            myWSModule.connections[user] = {iPad:null, Browser:connection};
        }else if(clientType == "iPad"){
            myWSModule.connections[user] = {iPad:connection, Browser:null};
        }
    }else{
        if(clientType == "browser"){
            if(myWSModule.connections[user].Browser != undefined){
                console.log("Old browser connectionclosed!");
                myWSModule.connections[user].Browser.close();
            }
            myWSModule.connections[user].Browser = connection;
        }else if(clientType == "iPad"){
            if(myWSModule.connections[user].iPad != undefined){
                console.log("Old iPad connection closed!");
                myWSModule.connections[user].iPad.close();
            }
            myWSModule.connections[user].iPad = connection;
        }
    }
};

myWSModule.findCookie = function(cookies, name){
    var cookieToFind = "";
    for(var i = 0; i < cookies.length; i++){
        if(cookies[i].name == name){
            cookieToFind = cookies[i];
        }
    }
    if(cookieToFind == ""){
        cookieToFind = null;
    }

    return cookieToFind;
};

myWSModule.createConnectionBindings = function(connection, device){
    if(device == "browser"){
       myWSModule.createBrowserBindings(connection);
    }else if(device == "iPad"){
        myWSModule.createIPadBindings(connection);
    }
};

myWSModule.createIPadBindings = function(connection){
  connection.on("message", function(msg){
     var message = JSON.parse(msg.utf8Data);
     if(message.type = "ItBSM"){
        if(myWSModule.connections[connection.ignUser].Browser != null){
            myWSModule.connections[connection.ignUser].Browser.send(JSON.stringify(message));
        }
     }
  });
};

myWSModule.createBrowserBindings = function(connection){
    connection.on("message", function(msg){
        var message = JSON.parse(msg.utf8Data);
        console.log(msg);
        if(message.type == "BtSR"){
            myWSModule.answerBrowserToServerRequest(connection);
        }else{
           if(myWSModule.iPadIsAvailable(connection.ignUser)){
                myWSModule.connections[connection.ignUser].iPad.send(JSON.stringify({type:"data", message:msg}));
            }
        }
    });

    connection.on("close", function(){
        if(myWSModule.connections[connection.ignUser].iPad != null){
            myWSModule.connections[connection.ignUser].iPad.send(JSON.stringify({type:"status", message:"closed"}));
        }else{
        }
    });
};

myWSModule.getClientType = function(req){
    var clientType = null;
    if(req.httpRequest.headers.clienttype != undefined){
        clientType = req.httpRequest.headers.clienttype;
    }
    return clientType;
};


myWSModule.answerBrowserToServerRequest = function(connection){
    if(myWSModule.iPadIsAvailable(connection.ignUser)){
        connection.send(JSON.stringify({type:"StBA", status:true}));
    }else{
        connection.send(JSON.stringify({type:"StBA", status:false}));
    }
}

myWSModule.iPadIsAvailable = function(user){
    if(myWSModule.connections[user].iPad != null){
        return true;
    }else{
        return false;
    }
}


module.exports = myWSModule;
