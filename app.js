
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var https = require('https');
var fs = require("fs");
var path = require('path');
var expSessions = require("express-session");
var bodyParser = require("body-parser");
var cuid = require("cuid");
var cookieParser = require("cookie-parser");
var myMiddleware = {};
var LoginManager = require("./routes/login");
var IGNApp = require("./routes/ignApp");
var redis = require("redis");
var redisClient = redis.createClient();
var RedisStore = require('connect-redis')(expSessions);



var sessionStore = new RedisStore({client:redisClient});
myMiddleware.checkAuth = function(req, res, next){
    console.log(req.session.user);
    if(!req.session.user){
        console.log(req.session);
        res.redirect("/");
    }else{
        console.log(req.session);
        next();
    }
};

var app = express();
var WSServer = require("./WSServer").init(sessionStore);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(parseCookie = cookieParser("paulscbgthisblabionadelemon"));
//app.use(cookieSession({secret:"paulscbgthisblabionadelemon", store:sessionStore}));
app.use(expSessions({
    genid: function(req) {
        return cuid(); // use UUIDs for session IDs
    },
    secret: "paulscbgthisblabionadelemon",
    store:sessionStore,
    resave: true,
    saveUninitialized: true
}));



app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, "keys")));
app.use(bodyParser.json());

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



app.get('/', routes.index);
app.get("/IGNApp",myMiddleware.checkAuth, IGNApp.loadPage);
app.get("/logout", LoginManager.logoutUser);
app.get("/signup", function(req, res){res.render("signup")});
app.post("/login", LoginManager.loginUser);
app.post("/createUser", LoginManager.createUser);


var certificate = fs.readFileSync("keys/server.crt");
var key = fs.readFileSync("keys/server.pem");

var httpsOptions = {
   cert:certificate,
   key: key
};

https.createServer(httpsOptions, app).listen(3001);
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

